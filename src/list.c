#include <stdio.h>
#include <stdlib.h>

struct cell_t {
  void* val;
  unsigned long int id;
  struct cell_t* next;
};
typedef struct cell_t* list_t;

list_t list_empty() {
  return NULL;
}

int list_is_empty(list_t l) {
  return l == NULL;
}

list_t list_push(list_t l, void* x) {
  list_t newCase = malloc(sizeof(struct cell_t));
  newCase->val = x;
  newCase->id = 1;
  newCase->next = NULL;
  if(list_is_empty(l)) {
    return newCase;
  }
  newCase->id = l->id+1;
  newCase->next = l;
  return newCase;
}

// renvoie la liste sans le premier element
list_t list_tail(list_t l) {
  return l->next;
}

// renvoie ce qui a dans la liste en premier et l'enleve de la liste
//donc ca supprime donc ca free 
//modifie la liste pour que elle pointe au suivant
void* list_pop(list_t* l) {
  if( list_is_empty(*l)) {
    return NULL;
  }
  /*
  struct cell_t* aRenvoyer = l->next;
  free(l);
  return aRenvoyer;
  */
  void * val = (*l)->val;
  list_t newL = (*l)->next;
  free(*l);
  *l = newL;
  return val;

  /*
  list_t liste = *l;
  void * val = list_top(liste);
  free(liste);
  */
  //*l = list_tail(liste);
  //return val;
}

//retourne le premier element (sa valeur ?)
void* list_top(list_t l) {
  return l-> val;
}

//detruit la liste 
//free_void permet de dealouer val 
void list_destroy(list_t l, void (*free_void)(void*)) {
  list_t currentCase;
  while(l != NULL) {
    currentCase = list_tail(l);
    free_void(l->val);
    free(l);
    l = currentCase;
  }
}

// return the found element or NULL
void* list_in(list_t l, void* x, int (*eq)(void*, void*)) {
  if (list_empty(l)) {
    return NULL;
  }
  list_t tmp = l;
  while( tmp != NULL) {
    if(eq(tmp->val,x)) {
      return tmp->val;
    }
    tmp = list_tail(tmp);
  }
  return NULL;
}

unsigned long int list_len(list_t l) {
  return l->id;
}

