#include <stdio.h>
#include <stdlib.h>

struct node_t {
  unsigned long int key;
  void* val;
  struct node_t* left;
  struct node_t* right;
};

typedef struct bst_t {
  // Use this to compute a key from a val
  // It is not really a hash function, the key must be unique
  unsigned long int (*h)(void*);

  unsigned long int size;
  struct node_t* tree;
} bst_t;

bst_t bst_empty(void) {
  bst_t t;
	t.size = 0;
	t.tree = NULL;
	return t;

}

int bst_is_empty(bst_t t) {
  return t.size == 0;
}

bst_t bst_add(bst_t t, void* x) {
  struct node_t* arbre = t.tree;
  if (arbre == NULL) {
    //Alors on va créer un arbre
    struct node_t * newArbre = malloc(sizeof(struct node_t));
    newArbre->key = 1;  // hash(x) ?
    newArbre->val = x;
    newArbre->right = NULL;
    newArbre->left = NULL;
    t.tree = newArbre;
  } 
  else {
    //Sinon faut aller a droite ou a gauche jusqua trouver le bon emplacement
    while (arbre != NULL) {
      if ()
    }
  }
}

void bst_destroy(bst_t t, void (*free_void)(void*)) { 

}

int bst_in(bst_t t, void* x) {

}

unsigned long int bst_size(bst_t t) {

}
